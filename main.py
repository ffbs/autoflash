import socket
import struct
import re
import requests

import devices

IFACE = 'wlp6s0'

def get_default_gateway_linux():
    """Read the default gateway directly from /proc."""
    with open("/proc/net/route") as fh:
        for line in fh:
            fields = line.strip().split()
            if fields[0] != IFACE or fields[1] != '00000000' or not int(fields[3], 16) & 2:
                continue
            return socket.inet_ntoa(struct.pack("<L", int(fields[2], 16)))

def handle_device(device):
    print('Detected as {}'.format(device.title))
    if device.flashable:
        print('flashable:', device.get_name())
    else:
        print('Device is not flashable')
        exit(3)

if __name__ == '__main__':
    gateway = get_default_gateway_linux()
    if not gateway:
        print('unable to detect default gateway')
        exit(1)
    print('checking default gateway', gateway)
    r = requests.get('http://{}/'.format(gateway))
    options = list(filter(lambda x: x.probe(gateway, r.text), [c() for c in devices.device_classes]))
    if len(options) == 0:
        print('Device not recognized')
        exit(2)
    elif len(options) == 1:
        handle_device(options[0])
    else:
        options = list(filter(lambda o: o.flashable, options))
        if len(options) == 0:
            print('Device is not flashable')
            exit(3)
        elif len(options) == 1:
            handle_device(options[0])
        else:
            # rule out parent options of other options to find most specific options
            bases = [base for o in options for base in o.__class__.__bases__]
            options = list(filter(lambda o: o.__class__ not in bases, options))
            if len(options) == 0:
                print('Inheritance filter failed, no options left')
                exit(3)
            elif len(options) == 1:
                handle_device(options[0])
            else:
                print('Still too many options')


