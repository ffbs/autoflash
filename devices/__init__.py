import inspect
import importlib
import pkgutil

class Device():
    title = 'AbstractDevice'
    flashable = False
    def probe(self, router_ip, indexpage):
        return False
    def get_name(self):
        return 'AbstractDevice'

device_classes = set()
__path__ = pkgutil.extend_path(__path__, __name__)
for importer, modname, ispkg in pkgutil.walk_packages(path=__path__, prefix=__name__+'.'):
    mod = importlib.import_module(modname)
    for _, clss in mod.__dict__.items():
        if inspect.isclass(clss) and issubclass(clss, Device):
            device_classes.add(clss)

