from devices import Device

class FritzDevice(Device):
    title = 'Generic AVM Fritz! Device'
    def probe(self, router_ip, indexpage):
        return 'FRITZ!' in indexpage

class FlashableFritzDevice(FritzDevice):
    title = 'FlashableFritzDevice'
    flashable = True

    flashable_versions = ['7362 SL']
    def probe(self, router_ip, indexpage):
        if super(FlashableFritzDevice, self).probe(router_ip, indexpage):
            m = re.search(r'"FRITZ!Box ([0-9][^"]+)"', indexpage)
            if m:
                self.version = m.group(1)
                return self.version in FlashableFritzDevice.flashable_versions
        return False

    def get_name(self):
        return 'FRITZ!Box {}'.format(self.version)
