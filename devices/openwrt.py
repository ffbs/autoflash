from devices import Device

class OpenwrtDevice(Device):
    title = 'Generic Openwrt Device'
    flashable = True
    def probe(self, router_ip, indexpage):
        return '/cgi-bin/luci' in indexpage
    def get_name(self):
        return 'OpenWRT Device'

