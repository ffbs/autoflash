from devices import Device

class TplinkDevice(Device):
    title = 'Generic TP-Link Device'
    def probe(self, router_ip, indexpage):
        return 'TP-Link' in indexpage

